# Drink with Docker

## Running Drink

The "official" image of Drink on the Docker Hub is
[bortzmeyer/drink](https://hub.docker.com/r/bortzmeyer/drink). 

You can start it with:

```
docker run -p 3553:53 -p 3553:53/udp bortzmeyer/drink 
```

(Assuming you want Drink to listen on port 3553.)

You will probably want to set at least two parameters, the base domain
and the nameserver name:

```
docker run -p 3553:53 -p 3553:53/udp bortzmeyer/drink --base foobar.example.org --nameserver ns1.example.net
```

Other Drink parameters (see `README.md` for a list) can be added.

If you run it this way, the `ip` service won't work properly since
[Docker will translate the IP
addresses](https://docs.docker.com/config/containers/container-networking/). To
avoid this, you can [run with](https://docs.docker.com/network/host/):

```
docker run --network host  bortzmeyer/drink
```

(In the future, Drink may use the [PROXYv2 protocol](https://framagit.org/bortzmeyer/drink/-/issues/39).

## Creating the Drink image

If you are a developer and want to recreate the image, just do:

```
docker build .
```

For an "official" image (`docker login` may be necessary for the
second step):

```
docker build --tag bortzmeyer/drink:latest .

docker push bortzmeyer/drink:latest
```
