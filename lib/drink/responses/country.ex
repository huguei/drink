defmodule Drink.Responses.Country do
  import Drink.Config
  import Drink.Utils
  require Drink.RCodes
  require Drink.Responses
  require Logger
  
  defmacro geoip_provider_ttl do
    3600
  end
  
  @spec geoip_get(String.t()) :: {atom, any}
  if Mix.target() == :http  or Mix.target() == :full do
    def geoip_get(addr) do
      GeoIP.lookup(addr)
    end
  else
    def geoip_get(_url) do
      {:nohttp, "Not built with HTTP client support, therefore no GeoIP"}
    end
  end

  @spec geoip_country(String.t()) :: {atom, any}
  def geoip_country(client) do
    result = geoip_get(client)
    case result do
      {:ok, data} ->
	country =
	if Map.has_key?(data, :country) do
	  data.country
	else
	  "No known country"
	end
	{:ok, country}
      {:error, reason} ->
	Logger.error("Cannot lookup #{client} at #{Drink.Defaults.geoip_provider}: #{inspect reason}")
	{:error, reason}
      {:nohttp, reason} ->
	Logger.error("Cannot lookup #{client} at #{Drink.Defaults.geoip_provider}: #{inspect reason}")
	{:error, reason}
    end	
  end
    
  def response(qtype, client) do
    if config()["services"]["country"] do
      case qtype do
	:txt ->
	  ip = a2s(client)
	  case geoip_country(ip) do
	    {:ok, country} ->
	      %{:rcode => Drink.RCodes.noerror,
		:ttl => geoip_provider_ttl(),
		:data => [country]}
	    {:error, _reason} ->
	      %{:rcode => Drink.RCodes.servfail, :ede => {23, "Cannot reach the geo-location server"}}
	    {:nohttp, reason} ->
	      Logger.warn("No HTTP: " <> reason)
	      %{:rcode => Drink.RCodes.servfail, :ede => {21, "No HTTP support, so no geolocation service"}}
	  end
	:any -> Drink.Responses.any_hinfo()
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end
