defmodule Drink.Responses.Bgp do
  import Drink.Config
  import Drink.Utils
  require Logger
  require Drink.RCodes
  require Drink.Responses

  @bgp_server "https://bgp.bortzmeyer.org/"
  defmacro bgp_server_ttl do
    3600 # Refresh of RIS data at this server
  end

  @spec http_get(String.t()) :: {atom, any}
  # By default, Mix.target() == host
  if Mix.target() == :http  or Mix.target() == :full do
    def http_get(url) do
      result = HTTPoison.get(url)
      case result do
	{:ok, %HTTPoison.Response{status_code: 200} = data} -> {:ok, data}
	{:ok, response} -> {:wrong_code, response}
	{:error, reason} -> {:error, reason} 
      end
    end
  else
    def http_get(_url) do
      {:nohttp, "Not built with HTTP client support"}
    end
  end

  @spec bgp_info(String.t()) :: {atom, any}
  def bgp_info(client) do
    result = http_get(@bgp_server <> client)
    case result do
      {:ok, data} ->
	{:ok, String.split(data.body, " ")}
      {:wrong_code, response} ->
	Logger.error("Wrong answer from #{@bgp_server}: #{inspect response}")
	{:error, response}
      {:nohttp, reason} ->
	Logger.error("Cannot reach #{@bgp_server}: #{inspect reason}")
	{:error, reason}
      {:error, reason} ->
	Logger.error("Cannot reach #{@bgp_server}: #{inspect reason}")
	{:error, reason}
    end
  end

  def response(qtype, client) do
    if config()["services"]["bgp"] do
      case qtype do
	:txt ->
	  ip = a2s(client)
	  case bgp_info(ip) do
	    {:ok, data} ->
	      %{:rcode => Drink.RCodes.noerror,
		:ttl => bgp_server_ttl(),
		:data => [ip | data]}
	    {:error, _reason} ->
	      %{:rcode => Drink.RCodes.servfail,
		:ede => {23, "Cannot reach the route server"}} # RFC 8914,
	      # section 4.24
	      {:nohttp, reason} ->
	      Logger.warn("No HTTP: " <> reason)
	      %{:rcode => Drink.RCodes.servfail,
		:ede => {21, "No HTTP support, so no route service"}}
	  end
	:any -> Drink.Responses.any_hinfo()
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end
