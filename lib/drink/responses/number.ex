if Mix.target() == :full do
  defmodule MyApp.Cldr do
    use Cldr,
      locales: [:ar, :ca, :de, :en, :es, :fr, :fr_BE, :fr_CH, :nl],
      providers: [Cldr.Number]
  end
end

defmodule Drink.Responses.Number do
  import Drink.Config
  require Logger
  require Drink.RCodes
  require Drink.Responses

  defmacro number_server_ttl do
    86400 # Reasonable duration for number change
  end

  @spec number_info(String.t(), integer) :: {atom, any}
  if Mix.target() == :full do
    def number_info(language, number) do
      MyApp.Cldr.Number.to_string(number, format: :spellout, locale: language)
    end
  else
    def number_info(_language, _number) do
      {:nocldr, "Not compiled in"}
    end
  end

  def query([lang, num, "number"]) do
    [lang, num, "number"] 
  end  
  def query([num, "number"]) do
    [nil, num, "number"] 
  end
  def query(["number"]) do
    [nil, nil, "number"] 
  end
    
  def response(qname, qtype) do
    if config()["services"]["number"] do
      [num, lang, "number"] = query(qname)
      if num == nil do
	%{:rcode => Drink.RCodes.noerror} # Probably QNAME minimization
      else
	case qtype do
	  :txt ->
	    case Integer.parse(num) do
	      {i, ""} ->
		case number_info(lang, i) do
		  {:ok, reply} ->
		    data = [reply]
		    %{:rcode => Drink.RCodes.noerror,
		      :ttl => number_server_ttl(),
		      :data => data}
		  {:error, reason} ->
		    %{:rcode => Drink.RCodes.servfail,
		      :ede => {21, "#{inspect reason}"}} # RFC 8914, section 4.22
		  {:nocldr, reason} ->
		    Logger.warn("No CLDR library: " <> reason)
		    %{:rcode => Drink.RCodes.servfail,
		  :ede => {21, "No CLDR support, so no number service"}}
		end
	      {_i, rest} ->
		%{:rcode => Drink.RCodes.servfail,
		  :ede => {21, "Cannot understand \"#{rest}\""}} # RFC 8914, section 4.22
	      :error ->
		%{:rcode => Drink.RCodes.servfail,
		  :ede => {21, "Cannot understand \"#{num}\""}} # RFC 8914, section 4.22		
	    end
	  :any -> Drink.Responses.any_hinfo()
	  _ -> %{:rcode => Drink.RCodes.noerror}
	end
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end
