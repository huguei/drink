defmodule Drink.Responses.Weather do
  import Drink.Config
  require Logger
  require Drink.RCodes
  require Drink.Responses

  @weather_base "http://api.weatherapi.com/v1/"
  defmacro weather_server_ttl do
    1800 # Reasonable duration for weather change
  end

  @spec http_get(String.t()) :: {atom, any}
  # By default, Mix.target() == host
  if Mix.target() == :http  or Mix.target() == :full do
    def http_get(url) do
      result = HTTPoison.get(url)
      case result do
	{:ok, %HTTPoison.Response{status_code: 200} = data} -> {:ok, data}
	{:ok, %HTTPoison.Response{status_code: 400} = data} -> {:nosuchcity, data}
	{:ok, response} -> {:wrong_code, response}
	{:error, reason} -> {:error, reason} 
      end
    end
  else
    def http_get(_url) do
      {:nohttp, "Not built with HTTP client support"}
    end
  end

  @spec get_future(map()) :: map()
  def get_future(data) do
    now = DateTime.utc_now()                                           
    next = DateTime.add(now, 86400)      
    {_list, result} =
      Enum.map_reduce(data["forecast"]["forecastday"], nil, fn d, _acc ->
	Enum.map_reduce(d["hour"], nil, fn h, acc ->
	  if abs(DateTime.diff(DateTime.from_unix!(h["time_epoch"]), next)) <=
            1800 do
            {:found, h}
	  else
            {nil, acc}
	  end
	end)
      end)
    result
  end
    
  @spec weather_info(String.t(), atom) :: {atom, any}
  def weather_info(city, time) do
    path =
    case time do
      :now -> "current.json?key=#{config()["weather-api-key"]}&q=#{city}"
      :tomorrow -> "forecast.json?key=#{config()["weather-api-key"]}&q=#{city}&days=2"
      _ -> raise ArgumentError, "Invalid time #{time}" 
    end
    result = http_get(@weather_base <> path)
    case result do
      {:ok, data} ->
	{:ok, data.body}
      {:nosuchcity, data} ->
	{:nosuchcity, data.body}
      {:wrong_code, response} ->
	Logger.error("Wrong answer from #{@weather_base}: #{inspect response}")
	{:error, response}
      {:nohttp, reason} ->
	Logger.error("Cannot reach #{@weather_base}: #{inspect reason}")
	{:error, reason}
      {:error, reason} ->
	Logger.error("Cannot reach #{@weather_base}: #{inspect reason}")
	{:error, reason}
    end
  end

  def query([city, stime, "weather"]) do
    [city, stime, :weather]
  end  
  def query([stime, "weather"]) do
    [nil, stime, :weather]
  end
  def query(["weather"]) do
    [nil, nil, :weather]
  end
  def query(_anything) do
    [nil, nil, nil]
  end
    
  def response(qname, qtype) do
    if config()["services"]["weather"] do
      [city, stime, weather] = query(qname)
      cond do
	weather != :weather ->
	  %{:rcode => Drink.RCodes.nxdomain}
	city == nil ->
	  %{:rcode => Drink.RCodes.noerror} # Probably QNAME minimization
	true ->
	  time =
	    case stime do
	      "now" -> :now
	      "tomorrow" -> :tomorrow
	      _ -> :invalid
	    end
	  case qtype do
	    :txt ->
	      if time == :invalid do
		%{:rcode => Drink.RCodes.nxdomain}
	      else
		case weather_info(city, time) do
		  {:ok, reply} ->
		    jdata = Jason.decode!(reply)
		    data =
		    if time == :now do
		      current = jdata["current"]
		      ["#{jdata["location"]["name"]}", "#{current["condition"]["text"]}",
		       "#{current["temp_c"]} C", "precipitation #{current["precip_mm"]} mm",
		       "wind #{current["wind_kph"]} km/h", "cloudiness #{current["cloud"]} %",
		       "humidity #{current["humidity"]} %"]
		    else
		      forecast = get_future(jdata)
		      ["#{jdata["location"]["name"]}", "#{forecast["condition"]["text"]}",
		       "#{forecast["temp_c"]} C", "precipitation #{forecast["precip_mm"]} mm",
		       "wind #{forecast["wind_kph"]} km/h", "cloudiness #{forecast["cloud"]} %",
		       "humidity #{forecast["humidity"]} %"]
		    end
		    %{:rcode => Drink.RCodes.noerror,
		      :ttl => weather_server_ttl(),
		      :data => data}
		  {:nosuchcity, _reason} ->
		    %{:rcode => Drink.RCodes.nxdomain}
		  {:error, _reason} ->
		    %{:rcode => Drink.RCodes.servfail,
		      :ede => {23, "Cannot reach the weather server"}} # RFC 8914,
		    # section 4.24
		    {:nohttp, reason} ->
		    Logger.warn("No HTTP: " <> reason)
		    %{:rcode => Drink.RCodes.servfail,
		      :ede => {21, "No HTTP support, so no weather service"}}
		end
	      end
	    :any -> Drink.Responses.any_hinfo()
	    _ -> %{:rcode => Drink.RCodes.noerror}
	  end
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end
