defmodule Drink.Responses.Full do
  import Drink.Config
  import Drink.Utils
  require Drink.RCodes
  require Drink.Responses
  require Drink.Responses.Bgp
  require Drink.Responses.Country
  require Logger
  
  @spec full_response(Drink.Utils.address) :: {atom, any}
  def full_response(client) do
    ip = a2s(client)
    country =
    case Drink.Responses.Country.geoip_country(ip) do
      {:ok, c} -> c
      {:error, _reason} -> "Unable to get the origin country"
      {:nohttp, reason} -> {:nohttp, reason}
    end
    case country
      do
      {:nohttp, reason} -> {:nohttp, reason}
      _ ->
	{prefix, as} =
	case Drink.Responses.Bgp.bgp_info(ip) do
	  {:ok, [prefix, as]} -> {prefix, as}
	  {:ok, [prefix, head | tail]} when tail != [] -> {prefix, Enum.join([head | tail], " ")} # We can have several AS numbers
	  {:error, _reason} -> {"Unknown AS", "Unable to get the BGP information"}
	end
      {:ok, [ip, prefix, as, country]}
    end
  end

  def response(qtype, client) do
    if config()["services"]["country"] && config()["services"]["bgp"] && config()["services"]["ip"] do
      case qtype do
	:txt ->
	  case full_response(client) do
	    {:ok, data} ->
	      %{:rcode => Drink.RCodes.noerror,
		:ttl => min(Drink.Responses.Country.geoip_provider_ttl, Drink.Responses.Bgp.bgp_server_ttl),
		:data => data}
	    {:nohttp, reason} ->
	      Logger.warn("No HTTP: " <> reason)
	      %{:rcode => Drink.RCodes.servfail, :ede => {21, "No HTTP support, so no geolocation service"}}
	  end
	:any -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end
