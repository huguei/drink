defmodule Drink.Responses.Op do
  import Drink.Config
  require Drink.RCodes
  require Drink.Responses
  @op_ttl 86400 # Arithmetics does not change
  def response(qname, qtype) do
    if config()["services"]["op"] do
      case qtype do
	:txt ->
	  cond do
	    length(qname) > 2 ->
	      %{:rcode => Drink.RCodes.nxdomain}
	    length(qname) == 1 ->
	      %{:rcode => Drink.RCodes.noerror, :ttl => @op_ttl,
		:data => ["Add an arithmetic expression such as '2+2'"]}
	    length(qname) == 2 ->
	      expr = Enum.at(qname, 0)
	      response =
		try do
		  Drink.Calculator.call(expr)
		rescue
		  _e in MatchError -> "syntax error in #{inspect expr}"
     		_e in ArithmeticError -> "arithmetic error in #{inspect expr}"
		  _e in FunctionClauseError -> "Sorry, #{inspect expr} triggered bug #44"
		end
	      %{:rcode => Drink.RCodes.noerror,
		:ttl => @op_ttl,
		:data => [to_charlist(response)]}
	  end
	:any -> Drink.Responses.any_hinfo
	_ -> %{:rcode => Drink.RCodes.noerror}
      end
    else
      %{:rcode => Drink.RCodes.nxdomain}
    end
  end
end
