defmodule Drink.RCodes do
  @moduledoc """
  The DNS library we use [has unfortunately no
  names](https://github.com/tungd/elixir-dns/issues/42) for DNS return
  codes. So, we do it ourselves. The authoritative list is [at
  IANA](https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-6).
  """

  defmacro noerror do
    0
  end
  defmacro formerr do 
    1
  end
  defmacro servfail do
    2
  end
  defmacro nxdomain do
    3
  end
  defmacro notimp do
    4
  end
  defmacro refused do
    5
  end

  def rcode_to_text(rcode) do
    case rcode do
      0 -> "NOERROR"
      1 -> "FORMERR"
      2 -> "SERVFAIL"
      3 -> "NXDOMAIN"
      4 -> "NOTIMP"
      5 -> "REFUSED"
      _ -> "#{rcode}"
    end
  end
  
end


  
