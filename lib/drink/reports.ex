defmodule Drink.Reports do
  use Agent # https://hexdocs.pm/elixir/Agent.html
  require Logger

  @report_delay 10000 # Milli-seconds
  @erase_threshold 1000 # Number of file dumps before restarting from zero
  @dump_file "drink-report.csv"
  
  def start_link() do
    report_pid = spawn_link(Drink.Reports, :dump_report, [])
    Process.register(report_pid, Drink.DumpReports)
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  def erase do
    Agent.update(__MODULE__, fn _state -> [] end)
  end
  
  def post(value) do
    labels = String.split(value, ".")
    if List.last(labels) == "report" do
      labels = List.delete_at(labels, length(labels)-1) # Remove report
      if List.first(labels) == "_er" and List.last(labels) == "_er" do
	labels = List.delete_at(List.delete_at(labels, length(labels)-1), 0) # Remove _er
	{error_code, ""} = Integer.parse(List.last(labels))
	{qtype, ""} = Integer.parse(List.first(labels))
	qname = Enum.join(List.delete_at(List.delete_at(labels, length(labels)-1), 0), ".")
	Agent.update(__MODULE__,
	  fn state ->
	    [{error_code, qtype, qname} | state]
	  end)
	"Thanks for the report of error #{error_code} on #{qname}"
      else
	# Else do nothing, probably QNAME minimization (or may be broken report)
	"Invalid report"
      end
    end
  end

  def get() do
    gen_report(Agent.get(__MODULE__, & &1))
  end

  def gen_report(data) do
    # We assume there are no qtype-dependant errors
    r = Enum.reduce(data, %{},
      fn {err, _type, name}, report ->
        if Map.has_key?(report, name) do
	   if err not in report[name] do
             Map.put(report, name, [err | report[name]])
	   else
	     report
	   end
	 else
	   Map.put(report, name, [err])
	 end
      end)
    Enum.map(r, fn record ->
      "#{elem(record, 0)}, #{Enum.join(elem(record, 1), ";")}"
    end)
  end

  def dump_report(n \\ 0) do
    f = File.open!(@dump_file, [:write])
    IO.puts(f, "REPORT state:")
    Enum.each(get(), fn l -> IO.puts(f, l) end)
    File.close(f)
    :timer.sleep(@report_delay)
    if n > @erase_threshold do
      erase()
      dump_report(0)
    else
      dump_report(n+1)
    end
  end
  
end
