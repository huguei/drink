defmodule Drink.Defaults do
  # If you modify default values, do not forget to update example.toml.
  @default_config %{"port" => 53, "base" => "test", "nameserver" => "ns1.test=::1", "maintainer" => "root.invalid",
		    "logging-facility" => "local0", "logging-level" => "warning", "console" => true,
		    "bind" => [:any], "negative-ttl" => 86400,
		    "server-bufsize" => 1440, "default-nonnull-ttl" => 30,
		    "secret-salt-for-cookies" => "I hate wasabi",
		    "dnssec_key" => nil, "dnssec_key_tag" => nil,
		    "ipc-socket" => nil,
		    "services" => ["date", "random", "ip", "hello", "ecs", "unit", "op"],
		    "ipv4-only" => false, "ipv6-only" => false, "max-random-value" => 1000,
		    "statistics" => false, # False by default since
					   # they can probably
					   # seriously slow down the
					   # server
		    "dot-cert" => nil, "dot-key" => nil, "dot-port" => 853,
		    "padding" => true,
                    # Now the IETF experimental stuff
		    "zoneversion" => false, # https://datatracker.ietf.org/doc/draft-ietf-dnsop-zoneversion/,
		    "reporting-agent" => nil, # https://datatracker.ietf.org/doc/draft-ietf-dnsop-dns-error-reporting/
		    "report-via-dns" => false}
  @config_file_home System.get_env("HOME") <> "/.drink.toml"
  @config_file_etc "/etc/drink.toml"
  def config_files do
     [@config_file_home, @config_file_etc]
  end
  def default_config do
    @default_config
  end
  def geoip_provider do
    :ipinfo # https://ipinfo.io/
  end
end
