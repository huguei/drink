defmodule Drink.RRTypes do
  @moduledoc """

  The DNS library we use has unfortunately no way to translate atoms such as :a or :txt to numeric values, but we need these values for DNSSEC. So, we do it ourselves. The authoritative list is [at
  IANA](https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-6).

  """

  def rrtype_to_num(type) do
    case type do
      :a -> 1
      :ns -> 2
      :cname -> 5
      :soa -> 6
      :null -> 10
      :hinfo -> 13
      :mx -> 15
      :txt -> 16
      :aaaa -> 28
      :naptr -> 35
      :srv -> 33
      :uri -> 256
      _ -> if is_integer(type), do: type, else: nil
    end
  end
  
  def rrtype_to_text(type) when is_atom(type) do
      "#{type}"
  end

  def rrtype_to_text(type) when is_integer(type) do
    case type do
      29 -> "loc"
      43 -> "ds"
      46 -> "rrsig"
      48 -> "dnskey"
      51 -> "nsec3param"
      59 -> "cds"
      60 -> "cdnskey"
      64 -> "svcb"
      65 -> "https"
      _ -> "#{type}"
    end
  end
  
end



  
