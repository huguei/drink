defmodule Drink.EdnsCodes do
  @moduledoc """
  The DNS library we use has unfortunately no [EDNS
  support](https://github.com/tungd/elixir-dns/issues/48). We list
  here the codes for EDNS options. The authoritative list is [at
  IANA](https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-11).
  """

  defmacro nsid do
    3
  end
  defmacro ecs do
    8
  end
  defmacro expire do
    9
  end
  defmacro cookie do
    10
  end
  defmacro padding do
    12
  end
  defmacro extended do
    15
  end
  defmacro zoneversion do
    65024 # 2022-08-21: Not officially registered in
	  # <https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-11>,
	  # this is for experimentation of
	  # draft-ietf-dnsop-zoneversion.
  end
  defmacro reporting do
    65023 # 2022-11-05: Not officially registered in
	  # <https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-11>,
	  # this is for experimentation of
	  # draft-ietf-dnsop-dns-error-reporting.
  end
  
end


  
