# Warning: if you modify this file, don't forget to also edit
# edns-erlang-after-or-equal-26.ex-link-it for versions of Erlang/OTP
# after 26. They are not automatically kept in sync.

defmodule Drink.EdnsError do
  defexception message: "Wrong encoding of EDNS options"
end

defmodule Drink.Edns do

  require Drink.EdnsCodes
  
  @spec extract_edns_opt(binary) :: []
  # Can raise ArgumentError or MatchError if the argument is not
  # proper EDNS options, for instance when using Binary.part. We
  # rescue it and raise our EdnsError instead.
  def extract_edns_opt(bin) do
    if byte_size(bin) != 0 do
      <<code::unsigned-integer-size(16)>> = Binary.part(bin, 0, 2)
      code_txt =
	case code do
	  Drink.EdnsCodes.nsid -> :nsid
	  Drink.EdnsCodes.ecs -> :ecs 
	  Drink.EdnsCodes.cookie -> :cookie 
	  Drink.EdnsCodes.padding -> :padding 
          Drink.EdnsCodes.zoneversion  -> :zoneversion
	  Drink.EdnsCodes.reporting  -> :reporting
	  other -> other
	end
      # Read RFC 6891
      <<length::unsigned-integer-size(16)>> = Binary.part(bin, 2, 2)
      data = Binary.part(bin, 4, length)
      [{code_txt, length, data} | extract_edns_opt(Binary.part(bin, 4+length, byte_size(bin)-(4+length)))]
    else
      []
    end
  rescue
    e ->
      raise Drink.EdnsError, inspect(e)
  end

  @type cookie :: nil | {binary, nil | binary}
  @type ecs :: nil | {Drink.Utils.address, integer}
  @type edns :: %{
    optional(:nsid) => boolean,
    optional(:ecs) => ecs,
    optional(:cookies) => cookie,
    optional(:padding) => boolean,
    # The rest is for IETF experimentations, not yet in RFCs
    optional(:zoneversion) => boolean,   
    optional(:reporting) => boolean
  }
  # In DNS requests, the additional section contains the OPT record
  # for EDNS. It takes a DNS section and returns a tuple with values
  # nil if no EDNS, the EDNS version if it is not zero, or the tuple
  # {version, bufsize, edns_options}
  @spec parse_additional(any) :: {nil} | {integer} | {integer, integer, edns, boolean}
  def parse_additional(r) do
    record = Enum.at(r, 0) # We assume a single record in the
    # additional section, which may be wrong.
    case record do 
      %DNS.ResourceOpt{data: opt_data, udp_payload_size: client_size, version: edns_version,
                       z: z} ->
	options = extract_edns_opt(opt_data)
	if edns_version == 0 do
	  # Erlang (and therefore the Elixir library we use) did not
	  # separate the DO field from the rest of the Z field
	  # <https://github.com/erlang/otp/issues/6605>. We therefore
	  # had to do it ourselves. It changed in Elixir 15 (OTP 26)
	  # so, afterwards, use instead the file
	  # edns-elixir-after-or-equal-15.ex-link-it.
	  <<dnssec_ok::1, _rest::15>> = Binary.pad_leading(Binary.from_integer(z), 2)
	  edns_options =
	    Enum.reduce(options, %{},
	      fn o, acc ->
		case elem(o, 0) do
		  :nsid -> Map.put(acc, :nsid, true)
		  :ecs -> Map.put(acc, :ecs, parse_ecs(o))
		  :cookie -> Map.put(acc, :cookies, parse_cookie(o))
		  :padding -> Map.put(acc, :padding, true) # We ignore
		  # the padding data in the request, RFC 7830, section
		  # 3.
		  :zoneversion -> Map.put(acc, :zoneversion, true)
		  :reporting -> Map.put(acc, :reporting, true)
		  _ -> acc
		end
	      end)
	  dnssec_ok =
	  if dnssec_ok == 0 do
	    false
	  else
	    true
	  end
	  {edns_version, client_size, edns_options, dnssec_ok}
	else # Unknown EDNS versions
	  {edns_version}
	end
      _ -> {nil}
    end 
  end

  @spec parse_ecs(boolean | nil | {any, any, any}) ::  ecs
  # No EDNS
  def parse_ecs(false) do
    nil
  end
  # No ECS option
  def parse_ecs(nil) do
    nil
  end 
  def parse_ecs(ecs) do
    # length = elem(ecs, 1)
    data = elem(ecs, 2)
    <<family::unsigned-integer-size(16)>> = Binary.part(data, 0, 2)
    family_txt =
      case family do
	# https://www.iana.org/assignments/address-family-numbers/address-family-numbers.xml#address-family-numbers-2
	1 -> :inet4
	2 -> :inet6
      end
    <<source_length::unsigned-integer-size(8)>> = Binary.part(data, 2, 1)
    <<scope_length::unsigned-integer-size(8)>> = Binary.part(data, 3, 1)
    address =
      case family_txt do
	:inet4 ->
	  data
	  |> Binary.part(4, 4)
	  |> extend_address(4)
	  |> Binary.to_list
	  |> List.to_tuple
	:inet6 ->
	  data
	  |> Binary.part(4, 16)
	  |> extend_address(16)
	  |> Binary.to_list
	  |> to_hextets
	  |> List.to_tuple
      end
    if scope_length != 0 do # Mandated by RFC 7871, section 6
      raise RuntimeError, message: "Invalid ECS"
    end
    {address, source_length}
  end
  
  # The address field in ECS is truncated by the source length.
  @spec extend_address(binary, integer) :: binary
  def extend_address(addr, length) do
    if byte_size(addr) < length do 
      Binary.pad_trailing(addr, length, 0)
    else
      addr
    end
  end

  # Erlang (and therefore Elixir) IPv6 addresses are tuples of hextets, not octets :-(
  @spec to_hextets([integer]) :: [integer]
  def to_hextets(l) do
    l
    |> Enum.chunk_every(2)
    |> Enum.map(fn x -> 256 * Enum.at(x, 0) + Enum.at(x, 1) end)
  end

  @spec parse_cookie(boolean | {:cookie, integer, binary}) ::  cookie
  def parse_cookie(false) do
    nil
  end
  def parse_cookie(nil) do
    nil
  end
  def parse_cookie({:cookie, length, data}) do
    if length < 8 do
      raise ArgumentError, "Wrong cookie length #{length}"
    end
    client_cookie = Binary.part(data, 0, 8)
    if length == 8 do # RFC 7873, section 4
      {client_cookie, nil}
    else
      if length < 16 or length > 40 do
	raise ArgumentError, "Wrong server cookie length #{length}"
      end
      server_cookie = Binary.part(data, 8, length-8)
      {client_cookie, server_cookie}
    end
  end

end
