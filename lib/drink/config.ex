defmodule Drink.Config do
  use Agent # https://hexdocs.pm/elixir/Agent.html

  def start_link(config) do
    Agent.start_link(fn -> config end, name: __MODULE__)
  end

  def config do
    Agent.get(__MODULE__, & &1)
  end

end
