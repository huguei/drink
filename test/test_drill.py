from data import expectations, timeout, server, dnssec_configs, number_configs

import subprocess
import os

PREFIX = "dnssec-tests"

if "DRINK_DOCKER" not in os.environ and \
   "DRINK_PROD" not in os.environ and \
   "DRINK_PROD2" not in os.environ: # No DNSSEC yet in Docker, and
                                    # test not yet adapted to
                                    # published domains (see #58)

    def test_basic():
        for config in dnssec_configs:
            fname = "%s/drink-drill-%s.out" % (PREFIX, config)
            out = open(fname, "w")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            print("Testing %s with key %s" % (domain, key), file=out)
            print("", file=out)
            out.flush()
            # See dnssec-tests/README.md
            p = subprocess.Popen(["drill", "-S", "-k", "%s/%s.key" % (PREFIX, key),
                                  "@%s" % server, "-p", str(port), "-t", "TXT", domain],
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
    def test_nodata():
        for config in dnssec_configs:
            fname = "%s/drink-drill-%s.out" % (PREFIX, config)
            out = open(fname, "a")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            p = subprocess.Popen(["drill", "-S", "-k", "%s/%s.key" % (PREFIX, key),
                                  "@%s" % server, "-p", str(port), "-t", "SRV", domain],
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
    def test_nodomain():
        for config in dnssec_configs:
            fname = "%s/drink-drill-%s.out" % (PREFIX, config)
            out = open(fname, "a")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            p = subprocess.Popen(["drill", "-S", "-k", "%s/%s.key" % (PREFIX, key),
                                  "@%s" % server, "-p", str(port), "-t", "TXT",
                                  "doesnotexist.%s" % domain],
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
    def test_nodomain_more_labels():
        for config in dnssec_configs:
            fname = "%s/drink-drill-%s.out" % (PREFIX, config)
            out = open(fname, "a")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            p = subprocess.Popen(["drill", "-S", "-k", "%s/%s.key" % (PREFIX, key),
                                  "@%s" % server, "-p", str(port), "-t", "TXT",
                                  "does.not.exist.%s" % domain],
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
    def test_number_unicode():
        # In some locales like DE (german), the answer in the number
        # service is not in pure ASCII, and may create DNSSEC issue if
        # we don't sign properly (sign the bytes, not the charlist)..
        for config in number_configs:
            fname = "%s/drink-drill-%s.out" % (PREFIX, config)
            out = open(fname, "w")
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            key = expectations[config]["key"]
            print("Testing %s with key %s" % (domain, key), file=out)
            print("", file=out)
            out.flush()
            # See dnssec-tests/README.md
            p = subprocess.Popen(["drill", "-S", "-k", "%s/%s.key" % (PREFIX, key),
                                  "@%s" % server, "-p", str(port), "-t", "TXT", "365.de.number." + domain],
                                 shell=False,
                                 stdout=out, stderr=subprocess.STDOUT)
            p.wait()
            out.close()
            assert (p.returncode == 0), \
                ("Invalid response (return code %s), see error messages in %s" % \
                 (p.returncode, fname))
        
