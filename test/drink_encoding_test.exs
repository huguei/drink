defmodule DrinkEncodingTest do
  use ExUnit.Case
  doctest Drink.Encoding

  # Binaries are encoded as themselves
  test "binary1" do
    assert Drink.Encoding.encode(<<3, 4, 5>>) == <<3, 4, 5>>
  end

  test "address1" do
    resource =  %DNS.Resource{type: :a, domain: "foo.bar", class: :in, ttl: 30, data: {1, 2, 3, 4}} 
    assert Drink.Encoding.encode(resource) == <<3, ?f, ?o, ?o, 3, ?b, ?a, ?r, 0, 1::unsigned-integer-size(16), 1::unsigned-integer-size(16), 30::unsigned-integer-size(32), 4::unsigned-integer-size(16), 1, 2, 3, 4>>
  end

  test "string1" do
    assert Drink.Encoding.encode_string("chocolat") == <<8, ?c, ?h, ?o, ?c, ?o, ?l, ?a, ?t>>
  end

  # With Unicode. There is no standard for text charset/encoding in
  # DNS text strings. We decide to use Unicode/UTF-8 but we cannot
  # guarantee all client will handle it (dig, for instance, does not
  # display it properly).
  test "string2" do
    assert Drink.Encoding.encode_string("café") == <<5, 0x63, 0x61, 0x66, 0xc3, 0xa9>>
  end
  
end
