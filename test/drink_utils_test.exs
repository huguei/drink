defmodule DrinkUtilsTest do
  use ExUnit.Case
  doctest Drink.Utils
  
  test "address1" do
    assert Drink.Utils.a2s({192, 0, 2, 33}) == "192.0.2.33"
    assert Drink.Utils.a2s({8193, 3512, 0, 0, 0, 0, 51966, 1}) == "2001:db8::cafe:1"
    assert Drink.Utils.a2s({0, 0, 0, 0, 0, 65535, 49152, 513}) == "192.0.2.1"
  end

end
