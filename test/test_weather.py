#!/usr/bin/env python3

import sys
import struct
import socket
import re
import os

import pytest

# DNSpython <https://www.dnspython.org/> <https://dnspython.readthedocs.io/>
import dns.rdatatype
import dns.message
import dns.query

from data import expectations, timeout, server, weather_configs
from utils import address_family

CITY = "Quimper"

def test_current():
    # WARNING: test will fail if there no outside connectivity.
    if "DRINK_PROD" in os.environ:
        for config in weather_configs:
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            message = dns.message.make_query(CITY + ".now.weather." + domain, dns.rdatatype.TXT)
            response = dns.query.udp(message, server, port=port, timeout=3*timeout) # The remote server can be slow.
            assert response.rcode() == dns.rcode.NOERROR and \
                response.answer[0][0].strings[5].decode().startswith("cloudiness")

def test_forecast():
    # WARNING: test will fail if there no outside connectivity.
    if "DRINK_PROD" in os.environ:
        for config in weather_configs:
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            message = dns.message.make_query(CITY + ".tomorrow.weather." + domain, dns.rdatatype.TXT)
            response = dns.query.udp(message, server, port=port, timeout=3*timeout) # The remote server can be slow.
            assert response.rcode() == dns.rcode.NOERROR and \
                "cloudiness" in response.answer[0][0].strings[5].decode()
