defmodule DrinkResponsesHelloTest do
  use ExUnit.Case
  doctest Drink.Responses.Hello
  test "a" do
    assert %{rcode: 0} = Drink.Responses.Hello.response(:a)
  end
  test "txt" do
    %{data: s, rcode: r} = Drink.Responses.Hello.response(:txt)
    assert r == 0 and is_list(s) and is_list(Enum.at(s, 0))
  end
end

