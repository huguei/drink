from data import expectations, timeout, server, http_configs, in_class, txt, nsid, noerror, formerr, opt, bufsize

import dns.message
import dns.query

import struct
import socket

# We generate deliberately broken DNS messages. We do not always
# expect an answer, we just want to check it does not break the
# server.

def encode_name(s):
    labels = s.split(".")
    result = b""
    for label in labels:
        result = result + struct.pack("B", len(label)) + label.encode()
    return result + struct.pack("B", 0)

id = 12345
misc = 0 # opcode 0, all flags zero 

def test_legal():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        addr = socket.getaddrinfo(server, port)[0] # Yes, we take only the
                                                   # first result, it is a
                                                   # bit arbitrary.
        (family, type, proto, canonname, sockaddr) = addr
        s = socket.socket(family, socket.SOCK_DGRAM) 
        s.settimeout(timeout)

        # This one is a legal message, to check that our testing code is correct.
        data = struct.pack(">HHHHHH", id, misc, 1, 0, 0, 0) + \
            encode_name("hello." + domain) + struct.pack(">H", txt) + struct.pack(">H", in_class)
        s.sendto(data, sockaddr)
        rdata, remote_server = s.recvfrom(4096)
        resp = dns.message.from_wire(rdata)
        assert resp.rcode() == noerror
        answers = resp.find_rrset(dns.message.ANSWER, dns.name.from_text("hello." + domain),
                                  in_class, txt)
        assert answers[0].strings[0].decode().startswith("Hello, this is the Drink")

def test_legal_edns():
    # A correct message, with an EDNS option
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        addr = socket.getaddrinfo(server, port)[0] # Yes, we take only the
                                                   # first result, it is a
                                                   # bit arbitrary.
        (family, type, proto, canonname, sockaddr) = addr
        s = socket.socket(family, socket.SOCK_DGRAM) 
        s.settimeout(timeout)
        edns_option = struct.pack(">H", nsid) + struct.pack(">H", 0)
        # Pseudo-RR OPT, RFC 6891, section 6.1.2
        additional_section =  struct.pack("B", 0) + struct.pack(">H", opt) + \
            struct.pack(">H", bufsize) + \
            struct.pack(">L", 0) + struct.pack(">H", 4) + \
            edns_option 
        data = struct.pack(">HHHHHH", id, misc, 1, 0, 0, 1) + \
            encode_name(domain) + struct.pack(">H", txt) + struct.pack(">H", in_class) + \
            additional_section
        s.sendto(data, sockaddr)
        rdata, remote_server = s.recvfrom(4096)
        resp = dns.message.from_wire(rdata)
        assert resp.rcode() == noerror

def test_missing():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        addr = socket.getaddrinfo(server, port)[0] # Yes, we take only the
                                                   # first result, it is a
                                                   # bit arbitrary.
        (family, type, proto, canonname, sockaddr) = addr
        s = socket.socket(family, socket.SOCK_DGRAM) 
        s.settimeout(timeout)
        data = struct.pack(">HHHHHH", id, misc, 1, 0, 0, 0) # The section
                                                            # question is
                                                            # missing
        s.sendto(data, sockaddr)
        # We don't expect an answer, so no recvfrom

def test_broken_edns():
        # Here, the DNS message is correct but the EDNS option is not. We
        # should receive a FORMERR (issue #33).
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        addr = socket.getaddrinfo(server, port)[0] # Yes, we take only the
                                                   # first result, it is a
                                                   # bit arbitrary.
        (family, type, proto, canonname, sockaddr) = addr
        s = socket.socket(family, socket.SOCK_DGRAM) 
        s.settimeout(timeout)
        edns_option = struct.pack(">H", nsid) + struct.pack(">H", 14) # Wrong length
        additional_section =  struct.pack("B", 0) + struct.pack(">H", opt) + \
            struct.pack(">H", bufsize) + \
            struct.pack(">L", 0) + struct.pack(">H", 4) + \
            edns_option 
        data = struct.pack(">HHHHHH", id, misc, 1, 0, 0, 1) + \
            encode_name(domain) + struct.pack(">H", txt) + struct.pack(">H", in_class) + \
            additional_section
        s.sendto(data, sockaddr)
        rdata, remote_server = s.recvfrom(4096)
        resp = dns.message.from_wire(rdata)
        assert resp.rcode() == formerr

        # A compression pointer pointing to itself (RFC 9267, section 2)
        data = struct.pack(">HHHHHHBB", id, misc, 1, 0, 0, 0, 0xc0, 0x0c)
        s.sendto(data, sockaddr)
        # We don't expect an answer, so no recvfrom

def test_nonquery():
    id = 12345
    misc = 32768 # opcode 0, all flags zero except the one which indicates this is a response, not a query. Should be ignored.
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        addr = socket.getaddrinfo(server, port)[0] # Yes, we take only the
                                                   # first result, it is a
                                                   # bit arbitrary.
        (family, type, proto, canonname, sockaddr) = addr
        s = socket.socket(family, socket.SOCK_DGRAM)
        s.settimeout(timeout)
        data = struct.pack(">HHHHHH", id, misc, 1, 0, 0, 0) + \
            encode_name(domain) + struct.pack(">H", txt) + struct.pack(">H", in_class)
        s.sendto(data, sockaddr)
        # We don't expect an answer, so no recvfrom

