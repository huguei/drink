from data import expectations, timeout, server, me, http_configs

import dns.message
import dns.query

def test_txt():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("hello." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and "this is the Drink DNS server" in str(response.answer[0][0])

def test_a():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("hello." + domain, dns.rdatatype.A)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and len(response.answer) == 0

def test_case():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        # 0x20 (case preservation)
        message = dns.message.make_query("heLlO." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and \
            "this is the Drink DNS server" in str(response.answer[0][0]) and \
            str(response.question[0]).startswith("heLlO." + domain)

