# Warning: if you modify this file, don't forget to also edit
# edns-test-erlang-after-or-equal-26.ex-link-it for versions of
# Erlang/OTP after 26. They are not automatically kept in sync.

defmodule DrinkEdnsTest do
  use ExUnit.Case
  doctest Drink.Edns

  test "edns1" do
    result = Drink.Edns.extract_edns_opt(<<3::unsigned-integer-size(16),0::unsigned-integer-size(16)>>)
    assert result == [{:nsid, 0, <<>>}]
  end

  test "edns2" do
    result = Drink.Edns.extract_edns_opt(<<3::unsigned-integer-size(16), 0::unsigned-integer-size(16), 42::unsigned-integer-size(16), 4::unsigned-integer-size(16), 666::unsigned-integer-size(32)>>)
    assert result == [{:nsid, 0, <<>>}, {42, 4, <<666::unsigned-integer-size(32)>>}]
  end

  # Invalid data
  test "edns3" do
    assert_raise Drink.EdnsError,
      fn -> Drink.Edns.extract_edns_opt(<<0::unsigned-integer-size(16), 4::unsigned-integer-size(16)>>) end
    assert_raise Drink.EdnsError,
      fn -> Drink.Edns.extract_edns_opt(<<0::unsigned-integer-size(16)>>) end
  end
  
  test "additional1" do
    result = Drink.Edns.parse_additional([])
    assert result == {nil}
  end
  
  test "additional2" do
    result = Drink.Edns.parse_additional(
      [%DNS.ResourceOpt{
	  data: <<>>,
	  domain: '.',
	  ext_rcode: 0,
	  type: :opt,
	  udp_payload_size: 1024,
	  version: 0,
	  z: 0}])
    assert result == {0, 1024, %{}, false} # No EDNS options
  end

  test "additional3" do
    result = Drink.Edns.parse_additional(
      [%DNS.ResourceOpt{
	  data: <<3::unsigned-integer-size(16), 0::unsigned-integer-size(16)>>,
	  domain: '.',
	  ext_rcode: 0,
	  type: :opt,
	  udp_payload_size: 1024,
	  version: 0,
	  z: 0}])
    assert result == {0, 1024, %{:nsid => true}, false} 
  end

  test "additional4" do
    result = Drink.Edns.parse_additional(
      [%DNS.ResourceOpt{
	  data: <<3::unsigned-integer-size(16), 0::unsigned-integer-size(16), 8::unsigned-integer-size(16), 7::unsigned-integer-size(16), 1::unsigned-integer-size(16), 24::unsigned-integer-size(8), 0::unsigned-integer-size(8), 192::unsigned-integer-size(8), 0::unsigned-integer-size(8), 2::unsigned-integer-size(8)>>,
	  domain: '.',
	  ext_rcode: 0,
	  type: :opt,
	  udp_payload_size: 768,
	  version: 0,
	  z: 0}])
    assert result == {0, 768, %{:ecs => {{192, 0, 2, 0}, 24}, :nsid => true}, false} 
  end

  test "dnssec1" do
    result = Drink.Edns.parse_additional(
      [%DNS.ResourceOpt{
	  data: <<3::unsigned-integer-size(16), 0::unsigned-integer-size(16), 8::unsigned-integer-size(16), 7::unsigned-integer-size(16), 1::unsigned-integer-size(16), 24::unsigned-integer-size(8), 0::unsigned-integer-size(8), 192::unsigned-integer-size(8), 0::unsigned-integer-size(8), 2::unsigned-integer-size(8)>>,
	  domain: '.',
	  ext_rcode: 0,
	  type: :opt,
	  udp_payload_size: 768,
	  version: 0,
	  z: 32768}])
    assert result == {0, 768, %{:ecs => {{192, 0, 2, 0}, 24}, :nsid => true}, true} 
  end
  
  test "ecs1" do
    result =  Drink.Edns.parse_ecs({0, 0, <<1::unsigned-integer-size(16), 24::unsigned-integer-size(8), 0::unsigned-integer-size(8), 192::unsigned-integer-size(8), 0::unsigned-integer-size(8), 2::unsigned-integer-size(8)>>})
    assert result == {{192, 0, 2, 0}, 24}
  end

  test "cookie1" do
    result = Drink.Edns.parse_cookie({:cookie, 8, <<1, 2, 3, 4, 5, 6, 7, 8>>})
    assert result == {<<1, 2, 3, 4, 5, 6, 7, 8>>, nil}
  end
  
  test "cookie2" do
    result = Drink.Edns.parse_cookie({:cookie, 16, <<1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16>>})
    assert result == {<<1, 2, 3, 4, 5, 6, 7, 8>>, <<9, 10, 11, 12, 13, 14, 15, 16>>}
  end
  
end
