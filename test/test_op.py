from data import expectations, timeout, server, me, http_configs

import dns.message
import dns.query

import re
import os

if "DRINK_PROD2" not in os.environ:

    def test_simple():
        for config in expectations:
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            message = dns.message.make_query("2+2.op." + domain, dns.rdatatype.TXT)
            response = dns.query.udp(message, server, port=port, timeout=timeout)
            assert response.rcode() == dns.rcode.NOERROR and \
                response.answer[0][0].strings[0].decode() == "4"

    def test_expr():
        for config in expectations:
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            message = dns.message.make_query("2 + 2 * 5 - 10 - 1.op." + domain, dns.rdatatype.TXT)
            response = dns.query.udp(message, server, port=port, timeout=timeout)
            assert response.rcode() == dns.rcode.NOERROR and \
                response.answer[0][0].strings[0].decode() == "1"

    def test_error():
        for config in expectations:
            domain = expectations[config]["domain"]
            port = expectations[config]["port"]
            message = dns.message.make_query("1/0.op." + domain, dns.rdatatype.TXT)
            response = dns.query.udp(message, server, port=port, timeout=timeout)
            assert response.rcode() == dns.rcode.NOERROR and \
                response.answer[0][0].strings[0].decode().startswith("arithmetic error")


