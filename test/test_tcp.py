from data import expectations, timeout, server, me, http_configs

import dns.message
import dns.query

import socket
import struct

# Unfortunately, it seems dnspython offers no way to send several DNS
# requests over the same TCP connection. See test_tcp_pipeline later.
def test_tcp_one():
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.TXT)
        response = dns.query.tcp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.NOERROR and \
            "Possible queries: " in str(response.answer[0][0])

def one_question(sock, qname, qtype):
    request = dns.message.make_query(qname, qtype)
    data = request.to_wire()
    sock.send(struct.pack(">H", len(data)))
    sock.send(data)
    return request.id

def one_response(sock):
    length = struct.unpack(">H", sock.recv(2))[0]
    data = sock.recv(length)
    response = dns.message.from_wire(data)
    return response

def test_tcp_pipeline():
    # dns.query.tcp does not allow to send several requests over a TCP
    # connection. So we have to do it manually. We do pipelining and can
    # handle servers replying out-of-order.
    for config in expectations:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        if config in http_configs:
            questions = [domain, "hello." + domain, "ecs." + domain]
        else:
            questions = [domain, "hello." + domain]
        addrinfo_list = socket.getaddrinfo(server, port)
        addrinfo = addrinfo_list[0] # We should try all the addresses but
        # we are lazy, we use only the first one.
        sock = socket.socket(addrinfo[0], socket.SOCK_STREAM)
        sock.settimeout(timeout)
        sock.connect(addrinfo[4],)
        ids = {}
        for name in questions:
            ids[one_question(sock, name, dns.rdatatype.TXT)] = name
        for i in range(len(questions)):
            response = one_response(sock)
            if response.id in ids: # We must use IDs to desambiguate
                # out-of-order replies (RFC 7766, section
                # 7)
                text = ""
                for s in response.answer[0][0].strings:
                    text += "%s " % s.decode()
                assert ids[response.id] + "." == str(response.question[0].name)
                del ids[response.id]
            else:
                assert False, "Unexpected ID %s" % response.id
        sock.shutdown(socket.SHUT_WR)

