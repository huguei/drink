from data import expectations, timeout, server, me, http_configs

import dns.message
import dns.query

def test_noecs():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("ecs." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and response.answer[0][0].strings[0].decode() == ""

def test_withecs():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("ecs." + domain, dns.rdatatype.TXT)
        message.use_edns(payload=4096, options=[dns.edns.ECSOption(address="192.0.2.0", srclen=24)])
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and response.answer[0][0].strings[0].decode() == "192.0.2.0/24"

