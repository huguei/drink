from data import expectations, timeout, server, me, http_configs, dot_configs

import dns.message
import dns.query

import OpenSSL

import socket
import struct
import ssl

def one_question(sock, qname, qtype, padding=False):
    request = dns.message.make_query(qname, qtype)
    if padding:
        request.use_edns(payload=4096, options=[dns.edns.GenericOption(dns.edns.PADDING, b'')])
    data = request.to_wire()
    sock.send(struct.pack(">H", len(data)))
    sock.send(data)
    return request.id

def one_response(sock):
    length = struct.unpack(">H", sock.recv(2))[0]
    data = sock.recv(length)
    response = dns.message.from_wire(data)
    return response

def setup_dot(server, port):
    context = ssl.create_default_context()
    context.set_alpn_protocols("dot")
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE  # Because the test certificate is self-signed
    sock = socket.create_connection((server, port))
    sock.settimeout(timeout)
    ssock = context.wrap_socket(sock,
                                server_hostname=server)
    return ssock

def test_not_by_default():
    for config in dot_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["dot-port"]
        ssock = setup_dot(server, port)
        one_question(ssock, domain, dns.rdatatype.TXT)
        response = one_response(ssock)
        assert response.rcode() == dns.rcode.NOERROR 
        for option in response.options:
            if option.otype == dns.edns.PADDING:
                assert False, "Padding found when not asking for it"
        assert True
                
def test_not_without_tls():
    for config in http_configs:
        if config in dot_configs:
            assert True
            return
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.TXT)
        message.use_edns(payload=4096, options=[dns.edns.GenericOption(dns.edns.PADDING, b'')])
        response = dns.query.tcp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.NOERROR 
        for option in response.options:
            if option.otype == dns.edns.PADDING:
                assert False, "Padding found when not using TLS"
        assert True
                
def test_has_padding():
    for config in dot_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["dot-port"]
        ssock = setup_dot(server, port)
        one_question(ssock, domain, dns.rdatatype.TXT, padding=True)
        response = one_response(ssock)
        assert response.rcode() == dns.rcode.NOERROR
        found = False
        for option in response.options:
            if option.otype == dns.edns.PADDING:
                assert len(response.to_wire()) > 460 # The reply is
                # large so will be padded to (1 or 2) * 486 bytes
                found = True
        assert found, "No padding when asking for it"
                
