import socket
import json

from data import ipc_socket, ipc_configs, stats_configs, expectations, server, timeout

import dns.rdatatype
import dns.message
import dns.query

def test_basic():
    for config in ipc_configs:
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.connect(ipc_socket)
        sock.send(b"status")
        r = sock.recv(4096).decode()
        assert r.startswith("Drink is running")
        sock.close()

def test_statistics():
    for config in stats_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query(domain, dns.rdatatype.SOA)
        response = dns.query.udp(message, server, port=port, timeout=timeout) 
        assert response.rcode() == dns.rcode.NOERROR 
        # Now, retrieve the statistics
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.connect(ipc_socket)
        sock.send(b"statistics")
        r = sock.recv(4096).decode()
        data = json.loads(r)
        assert data["protocols"]["udp"] > 0 and data["rcodes"]["NOERROR"] > 0
        sock.close()
        
