import subprocess
import os

import pytest

from data import run_timeout 

free_port = "8353" # Just be sure it is available, otherwise the test will fail.

if "DRINK_PROD" not in os.environ and "DRINK_PROD2" not in os.environ and "DRINK_DOCKER" not in os.environ:
    
    def test_not_option():
        p = subprocess.run(["mix", "run", "drink.exs", "--unexisting-option"], timeout=run_timeout, shell=False)
        assert p.returncode == 1 

    def test_syntax():
        p = subprocess.run(["mix", "run", "drink.exs", "not-correct-option"], timeout=run_timeout, shell=False)
        assert p.returncode == 1

    def test_incorrect_option():
        p = subprocess.run(["mix", "run", "drink.exs", "--port", free_port,
                            "-b", "ok.test", "-n", "ns1.ok.test"], # Name server in bailiwick,
                                                                   # but no IP address.
                               timeout=run_timeout, shell=False)
        assert p.returncode == 1

    def test_correct():
        with pytest.raises(subprocess.TimeoutExpired):
            p = subprocess.run(["mix", "run", "drink.exs", "--port", free_port], timeout=run_timeout, shell=False)

    def test_correct_option():
        with pytest.raises(subprocess.TimeoutExpired):
            p = subprocess.run(["mix", "run", "drink.exs", "--port", free_port, "-b", "ok.test", "-n", "ns1.ok.test=2001:db8:1::53"],
                               timeout=run_timeout, shell=False)

