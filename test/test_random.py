from data import expectations, timeout, server, me, http_configs

import dns.message
import dns.query

import re

def test_txt():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("random." + domain, dns.rdatatype.TXT)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and \
            re.search(r"^[0-9]+$",
                      response.answer[0][0].strings[0].decode()) is not None

def test_a():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("random." + domain, dns.rdatatype.A)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and \
            re.search(r"^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$", response.answer[0][0].address) is not None

def test_aaaa():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("random." + domain, dns.rdatatype.AAAA)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and \
            re.search(r"^[0-9a-f:]+$", response.answer[0][0].address) is not None

def test_loc():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("random." + domain, dns.rdatatype.LOC)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR

def test_uri():
    for config in http_configs:
        domain = expectations[config]["domain"]
        port = expectations[config]["port"]
        message = dns.message.make_query("random." + domain, dns.rdatatype.URI)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and re.search(r"www.openstreetmap.org",
                      response.answer[0][0].target.decode()) is not None
        # Bug #46 is triggered only when generating DNSSEC signatures
        message = dns.message.make_query("random." + domain, dns.rdatatype.URI,
                                         want_dnssec=True)
        response = dns.query.udp(message, server, port=port, timeout=timeout)
        assert response.rcode() == dns.rcode.NOERROR and re.search(r"www.openstreetmap.org",
                      response.answer[0][0].target.decode()) is not None
