For the DNS tests by an external program in Python, run
`pytest`. You'll need the Python packages `dnspython` and `pyopenssl`.
It is *highly* recommended to run `./test.sh` in the parent directory
before, to run internal tests, and to check that everything is
compiled.

