# Makefile only useful for development

all:
	@echo "${MAKE} run" to run the server

links: lib/drink/edns.ex

lib/drink/edns.ex:
	./create-links.sh

compile: links
	mix deps.get
	mix deps.compile
	mix compile

update:
	mix deps.update --all
	mix deps.compile
	mix compile

upgrade: update

check: compile
	mix dialyzer

run:	links
	mix run drink.exs

escript: links
	mix escript.build

# See https://blog.appsignal.com/2022/03/15/a-guide-to-secure-elixir-package-updates.html
deptree:
	mix deps.tree --format dot && dot -Tpng deps_tree.dot -o deps_tree.png

signed:
	mix run drink.exs --dnssec-key dnssec-tests/Ktest.+008+18307.pem --dnssec-key-tag 18307 

clean:
	find . -name '*~' | xargs rm -f

distclean: clean
	rm -rf _build deps mix.lock

dist: distclean
	(cd ..; tar cvf drink.tar drink)

